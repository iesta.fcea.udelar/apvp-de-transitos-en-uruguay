if (!require(pacman)) {
  install.packages("pacman")
}

pacman::p_load(
    shiny,
    tidyverse,
    magrittr,
    shinyWidgets,
    shinydashboard,
    here,
    readxl,
    sf,
    leaflet,
    leaflet.extras,
    data.table,
    lubridate,
    ggimage,
    rsvg,
    ggthemes,
    hrbrthemes
)

load(
    here(
        "R",
        "Data.RData"
    )
)


data %<>%
    mutate(
        icono = recode(
            Vehiculo,
                "Moto" = here("www", "Icons", "motorcycle-solid.svg"),
                "Auto" = here("www", "Icons", "car-solid.svg"),
                "Camioneta" = here("www", "Icons", "truck-pickup-solid.svg"),
                "Peaton" = here("www", "Icons", "walking-solid.svg"),
                "Bicicleta" = here("www", "Icons", "bicycle-solid.svg"),
                "Caballo" = here("www", "Icons", "horse-solid.svg"),
                "Camion" = here("www", "Icons", "truck-solid.svg"),
                "Omnibus" = here("www", "Icons", "bus-solid.svg"),
                "Tractor" = here("www", "Icons", "tractor-solid.svg")
        )
    ) %>%
    data.table()

Icon <- iconList(
    "Moto" = makeIcon(
        iconUrl = here("www", "Icons", "motorcycle-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Auto" = makeIcon(
        iconUrl = here("www", "Icons", "car-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Camioneta" = makeIcon(
        iconUrl = here("www", "Icons", "truck-pickup-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Peaton" = makeIcon(
        iconUrl = here("www", "Icons", "walking-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Bicicleta" = makeIcon(
        iconUrl = here("www", "Icons", "bicycle-solid.svg"),
        iconHeight = 10,
        iconWidth = 10
    ),
    "Caballo" = makeIcon(
        iconUrl = here("www", "Icons", "horse-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Camion" = makeIcon(
        iconUrl = here("www", "Icons", "truck-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Omnibus" = makeIcon(
        iconUrl = here("www", "Icons", "bus-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    ),
    "Tractor" = makeIcon(
        iconUrl = here("www", "Icons", "tractor-solid.svg"),
        iconWidth = 10,
        iconHeight = 10
    )
)