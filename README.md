# Shiny App : Años de vida perdidos por siniestros de tránsito en Uruguay (2013-2019)
[![](https://img.shields.io/badge/Shiny-shinyapps.io-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)](https://gimese-iesta-udelar.shinyapps.io/APVPTransito/)
[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/iesta.fcea.udelar/apvp-de-transitos-en-uruguay)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-curved)](https://gitlab.com/iesta.fcea.udelar/apvp-de-transitos-en-uruguay/-/issues)


En este repositorio se encuentra el código de R de la aplicación [Shiny](https://gimese-iesta-udelar.shinyapps.io/APVPTransito/). 

# Reproducibilidad

Para hacer reproducible el código se incluyeron los datos procesados para ejecutar la Shiny pero NO se incluyerón los datos crudos,
estos fuerón obtenidos de la página de la [UNASEV](https://www.gub.uy/unidad-nacional-seguridad-vial/) y tablas de mortalidad del [INE](https://www.ine.gub.uy/).

## Gitpod 

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/iesta.fcea.udelar/apvp-de-transitos-en-uruguay)

## Rstudio Cloud

<a href="https://rstudio.cloud/project/2624888">
    <img src="https://www.tidyverse.org/rstudio-logo.svg" alt="RStudio" height="48">
</a>

## Reproducir en tu propia computadora 

### Como utilizar p_load() del paquete pacman

Para dar cierta reproducibilidad a nuestro trabajo en Shiny utilizamos la librería Pacman, que entre otras cosas nos ayuda a hacer nuestro código reproducible.
En nuestro caso al utilizar varias librerías (algunas quizás no tan comunes) utilizamos la función p_load para en el caso que en su espacio de trabajo no cuente con alguna de ellas no tenga que instalarla manualmente. 
**Hay que tener consideración que si utiliza RStudio y al abrir el script "app.R" y al seleccionar "Run App" puede ser que no funcione.**. A continuación, vamos a explicitar como ejecutar la aplicación: 

1. *Ejecute las lineas indicadas en el script app.R* 
![Captura de Pantalla](img/RStudioCloud.png)
2. *En el paso anterior, R instalara las librerias que no tenga instaldas para correr la aplicación, si quiere ver mas el detalle puede buscar el script **Config.R** que se encuentra en la carpeta R. El proceso depende de la velocidad de su maquina y conexión a internet y las librerias que ya tenga instaladas.* 
3. *Verifique que el paso anterior no tenga errores.*
    Revise que los paquetes se instalen, el último es **hrbrthemes**
![Captura de Pantalla](img/RStudioCloudTerminado.png)
La advertencia referida a Pacman no es de importancia en el caso que se encuentren mensajes de que los demás paquetes fuerón instalados correctamente, en caso contrario vuelva a ejecutar las sentencias.
4. *En el caso que utilice R Studio debería de poder ejecutar la aplicación desde el botón "Run App" o si utiliza otro IDE poder ejecutar el script completo.*

**La reproducibilidad fue testeada utilizando RStudio Cloud con un proyecto vacío (sin librerías instaladas) y clonado directamente desde este repositorio.**


**Los resultados fuerón satisfactorios, pero ante cualquier inconveniente puede comunicarse con nosotros.**

# Capturas de pantalla 

## Bienvenida
![Captura de Pantlla](img/webpage.png)

## Georeferencias de siniestros de tránsito mortales 
![Mapa de Siniestros de tránsito](img/wpmapa.png)

## Años de vida perdidos por edad 
![Años de vida perdidos por edad](img/wpapvpedad.png)

## Aplicar filtros
![Años de vida perdidos por edad filtros](img/wpapvpedadfil.png)

# ¡Gracias!
