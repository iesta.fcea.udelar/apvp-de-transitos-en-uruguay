FROM rocker/r-ver:latest
FROM rocker/geospatial:latest

# Paquetes de R
RUN install2.r --error \
    pacman \
    shiny \
    tidyverse \
    magrittr \
    shinyWidgets \
    shinydashboard \
    here \
    readxl \
    sf \
    leaflet \
    leaflet.extras \
    data.table \
    lubridate \
    ggimage \
    rsvg \
    ggthemes \
    hrbrthemes